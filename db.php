<?php
 $usuario = $argv[1];
  $contra = $argv[2];
  $db = $argv[3];
  $server = $argv[4];
  $tabla = $argv[5];
  $filename = $argv[6];

  $dbServer = "$server";
  $dbUserName = "$usuario";
  $dbPassword = "$contra";
  $dbName = "$db";
  $dbPort = 3306;

  $connection = new mysqli($dbServer, $dbUserName, $dbPassword, $dbName, $dbPort);

	// /* check connection */
	if ($connection->connect_errno) {
	     echo "Error: $connection->connect_error\n";
        exit;
	    
	}

$sql = "SELECT * FROM $tabla;";
$result = $connection->query($sql);

if ($result) {
  $fp = fopen($filename, 'w');
  // add header
  fputcsv($fp, ['nombre', 'apellido', 'email', 'edad']);
  while($user_record = $result->fetch_assoc()){
    fputcsv($fp, $user_record);
  }
}
fclose($fp);